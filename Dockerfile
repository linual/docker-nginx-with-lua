FROM debian:9 as build

ENV LUAJIT_LIB=export LUAJIT_LIB=/usr/local/luajit/lib
ENV LUAJIT_INC=export LUAJIT_INC=/usr/local/luajit/include/luajit-2.0
ENV LUAJIT=2.0.5
ENV NGX_DEVEL_KIT=v0.3.0
ENV LUA_NGINX_MODULE=v0.10.9rc7
ENV NGINX=nginx-1.19.3

RUN apt update && apt install -y wget gcc make libpcre3-dev zlib1g-dev libluajit-5.1-dev libssl-dev
RUN wget http://luajit.org/download/LuaJIT-${LUAJIT}.tar.gz && tar xvfz LuaJIT-2.0.5.tar.gz && cd LuaJIT-2.0.5 && make && make install PREFIX=/usr/local/luajit
RUN echo $LUAJIT_LIB && echo $LUAJIT_INC
RUN wget https://github.com/simpl/ngx_devel_kit/archive/${NGX_DEVEL_KIT}.tar.gz && tar xvfz v0.3.0.tar.gz
RUN wget https://github.com/openresty/lua-nginx-module/archive/${LUA_NGINX_MODULE}.tar.gz && tar xvfz v0.10.9rc7.tar.gz 
RUN wget https://openresty.org/download/${NGINX}.tar.gz && tar xvfz nginx-1.19.3.tar.gz && cd nginx-1.19.3 && ./configure --prefix=/usr/local/nginx --with-ld-opt="-Wl,-rpath,/usr/local/luajit/lib" --add-module=/ngx_devel_kit-0.3.0 --add-module=/lua-nginx-module-0.10.9rc7 && make -j2 && make install

FROM debian:9
WORKDIR /usr/local/nginx/sbins
RUN echo $LUAJIT_LIB && echo $LUAJIT_INC
COPY --from=build /usr/local/luajit/ /usr/local/luajit
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]
